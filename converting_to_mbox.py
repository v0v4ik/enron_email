import re
import email
from time import asctime
import os
import sys
from dateutil.parser import parse  

directory = sys.argv[1]

for (root, dirs, file_names) in os.walk(directory):

    for file_name in file_names:
        if file_name == "DELETIONS.txt":
            continue
        file_path = os.path.join(root, file_name)

        message_text = open(file_path).read()

        _from = re.search(r"From: ([^\r]+)", message_text).groups()[0]
        _date = re.search(r"Date: ([^\r]+)", message_text).groups()[0]

        _date = asctime(parse(_date).timetuple())

        msg = email.message_from_string(message_text)
        msg.set_unixfrom('From %s %s' % (_from, _date))
        print msg.as_string(unixfrom=True)
        print
